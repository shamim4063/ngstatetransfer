import { Injectable,Injector } from '@angular/core';
import { BaseService } from '../shared/service/base.service';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailService extends BaseService {

  constructor(injector: Injector) {
    super(injector);
  }
  getProductData(slug) {
    return this.http.get(`${this.apiUrl}product/${slug}`);
  }
}
