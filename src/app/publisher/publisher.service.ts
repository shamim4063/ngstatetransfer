import { Injectable, Injector } from '@angular/core';
import { BaseService } from '../shared/service/base.service';
@Injectable({
  providedIn: 'root'
})
export class PublisherService extends BaseService {

  constructor(injector: Injector) {
    super(injector);
  }

  getPublisherData() {
    return this.http.get(`${this.apiUrl}publishers`);
  }

}
