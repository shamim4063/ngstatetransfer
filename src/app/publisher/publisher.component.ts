import { Component, OnInit } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';
const PUBLISHER_KEY = makeStateKey('publisherkey');

import {PublisherService} from './publisher.service';

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.css']
})
export class PublisherComponent implements OnInit {

  publishers: any[] = [];
  pubData: any;

  constructor(private _publisherService: PublisherService, private state: TransferState) { }

  ngOnInit() {
    this.pubData = this.state.get(PUBLISHER_KEY, null as any);

    if (!this.pubData) {
      this._publisherService.getPublisherData().subscribe(
        (publisher: any) => {
          this.publishers = publisher.data;
          this.state.set(PUBLISHER_KEY, publisher as any);         
        },
        err => console.error(err),
        () => console.log('Publisher Loading Finish')
      )
    }else {
      this.publishers = this.pubData.data;
    }
  }

}
