import { Injectable, Injector } from '@angular/core';
import { BaseService } from '../shared/service/base.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorService extends BaseService {

  constructor(injector: Injector) {
    super(injector);
  }

  getAuthorData() {
    return this.http.get(`${this.apiUrl}authors`);
  }

}
