import { Component, OnInit } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';
const AUTHOR_KEY = makeStateKey('authorkey');

import { AuthorService } from './author.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  authors: any[] = [];
  authData: any;

  constructor(private _authorService: AuthorService, private state: TransferState) { }

  ngOnInit() {
    this.authData = this.state.get(AUTHOR_KEY, null as any);

    if (!this.authData) {
      this._authorService.getAuthorData().subscribe(
        (author: any) => {
          this.authors = author.data;
          this.state.set(AUTHOR_KEY, author as any);         
        },
        err => console.error(err),
        () => console.log('Author Loading Finish')
      )
    }else {
      this.authors = this.authData.data;
    }
  }

}
