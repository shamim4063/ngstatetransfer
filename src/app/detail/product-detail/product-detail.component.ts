import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { TransferState, makeStateKey } from '@angular/platform-browser';
const PRODUCT_DETAIL = makeStateKey('productdetail');

import { ProductDetailService } from './product-detail.service';
import { SeoContentService } from '../../shared/service/seo-content.service';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product_slug: string;
  product: any = {};
  pData: any;
  public isBrowser: boolean;

  constructor(
    private route: ActivatedRoute,
    private _productDetailService: ProductDetailService,
    private state: TransferState,
    @Inject(PLATFORM_ID) private platformId: Object,
    private _seoContentService: SeoContentService
  ) {
    if (isPlatformBrowser(this.platformId))
      this.isBrowser = true;
  }

  ngOnInit() {
    this.product_slug = this.route.snapshot.params['slug'];
    this.pData = this.state.get(PRODUCT_DETAIL, null as any);
    if (!this.pData) {
      this._productDetailService.getProductData(this.product_slug).subscribe(
        (data: any) => {
          this.product = data.product;
          this._seoContentService.setContent(this.product.name, `${this.product} Book Description`, "Book,Keyword,Book,Store", this.product.seo_url, this.product.image);
        },
        err => console.error(err),
        () => console.log('Product Loading Finish')
      )
    } else {
      this.product = this.pData.product;
      this._seoContentService.setContent(this.product.name, `${this.product} Book Description`, "Book,Keyword,Book,Store", this.product.seo_url, this.product.image);
    }
  }

  ngOnDestroy() {
    if (this.isBrowser) {
      console.log("Destroy Product Detail Component");
      this.state.remove(PRODUCT_DETAIL);
    }
  }

}
