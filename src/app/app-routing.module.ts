import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AuthorComponent } from './author/author.component';
import { CategoryComponent } from './category/category.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductComponent } from './product/product.component';
import { PublisherComponent } from './publisher/publisher.component';


const routes: Routes = [
    {
        path: '', component: HomeComponent
    },
    {
        path: 'author', component: AuthorComponent
    },
    {
        path: 'publisher', component: PublisherComponent
    },
    {
        path: 'category', component: CategoryComponent
    },
    {
        path: 'book/:slug', component: ProductDetailComponent
    },
    {
        path: 'lazy-book/:slug', loadChildren:'./detail/detail.module#DetailModule'
    },
    {
        path: ':type/:seo_url', component: ProductComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
