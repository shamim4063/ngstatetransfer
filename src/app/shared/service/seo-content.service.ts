import { Injectable, Inject } from '@angular/core';
import { Title, Meta, DOCUMENT } from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
  })
export class SeoContentService {

    constructor(private titleService: Title, private metaService: Meta, @Inject(DOCUMENT) private document) { }

    setContent(title: string, description: string, keywords: string, slug: string = '', image: string = '', update = true) {
        let tags = {
            title: title && title != '' && update ? title : "Boibazar Largest Online Book Store in Bangladesh",
            description: description && description != '' && update ? description : "Buy Books & Get Home Delivery anywhere in Bangladesh",
            keywords: keywords && keywords != '' && update ? keywords : "Books, Boibazar, Boimela, Book, Bangladesh, Largest, Big, Boro, Bishal, Online, Shop, Place",
            image: image
        }

        this.titleService.setTitle(tags.title);

        this.metaService.updateTag({ name: 'twitter:card', content: 'Product' });
        this.metaService.updateTag({ name: 'twitter:site', content: 'Boibazar.com' });
        this.metaService.updateTag({ name: 'twitter:title', content: tags.title });
        this.metaService.updateTag({ name: 'twitter:description', content: tags.description });
        this.metaService.updateTag({ name: 'twitter:image', content: `http://boibazar.com/${tags.image}` });
        
        this.metaService.updateTag({ property: 'og:type', content: 'Product' });
        this.metaService.updateTag({ property: 'og:site_name', content: 'Boibazar.com' });
        this.metaService.updateTag({ property: 'og:title', content: tags.title });
        this.metaService.updateTag({ property: 'og:description', content: tags.description });
        this.metaService.updateTag({ property: 'og:image', content: `http://boibazar.com/${tags.image}` });
        this.metaService.updateTag({ property: 'og:url', content: `http://boibazar.com/book/${slug}` });
    }
}