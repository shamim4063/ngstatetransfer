import { Injector, Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

import { BaseCalss } from '../classes/base-config.class';

@Injectable()
export class BaseService {
    protected http: HttpClient;
    conf=new BaseCalss();
    
    get apiUrl() {
        return this.conf.baseApiUrl;
    }
    constructor(injector: Injector) {
        this.http = injector.get(HttpClient);
    }
}