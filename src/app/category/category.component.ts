import { Component, OnInit } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';
const CATEGORY_KEY = makeStateKey('categorykey');

import { CategoryService } from './category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories: any[] = [];

  constructor(private _categoryService: CategoryService, private state: TransferState) { }

  ngOnInit() {
    this.categories = this.state.get(CATEGORY_KEY, null as any);

    if (!this.categories) {
      this._categoryService.getCategoryData().subscribe(
        (data: any) => {
          this.categories = data;
          this.state.set(CATEGORY_KEY, data as any);         
        },
        err => console.error(err),
        () => console.log('Category Loading Finish')
      )
    }
  }
}
