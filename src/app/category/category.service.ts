import { Injectable, Injector } from '@angular/core';
import { BaseService } from '../shared/service/base.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends BaseService {

  constructor(injector: Injector) {
    super(injector);
  }
  getCategoryData() {
    return this.http.get(`${this.apiUrl}categories?featured=0`);
  }
}
