import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { TransferState, makeStateKey } from '@angular/platform-browser';
const PRODUCT_LIST = makeStateKey('productlist');

import { ProductService } from './product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  search_url: string;
  search_type: string;
  products: any[] = [];
  pData: any;
  public isBrowser: boolean;

  constructor(
    private route: ActivatedRoute,
    private _productService: ProductService,
    private state: TransferState,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    if (isPlatformBrowser(this.platformId))
      this.isBrowser = true;
  }

  ngOnInit() {
    this.search_type = this.route.snapshot.params['type'];
    this.search_url = this.route.snapshot.params['seo_url'];
    this.pData = this.state.get(PRODUCT_LIST, null as any);

    if (!this.pData) {
      this._productService.getProductList(this.search_type, this.search_url).subscribe(
        (data: any) => {
          this.state.set(PRODUCT_LIST, data as any);
          this.products = data.products;
        },
        err => console.error(err),
        () => console.log('Product List Loading Finish')
      )
    } else {
      this.products = this.pData.products;
    }
  }

  ngOnDestroy() {
    if (this.isBrowser) {
      console.log("Destroy Product Component");
      this.state.remove(PRODUCT_LIST);
    }
  }

}
