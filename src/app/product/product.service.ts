import { Injectable, Injector } from '@angular/core';
import { BaseService } from '../shared/service/base.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseService{

  constructor(injector: Injector) {
    super(injector);
  }
  getProductList(product_of, slug) {
    return this.http.get(`${this.apiUrl}${product_of}/${slug}`);
  }

}
