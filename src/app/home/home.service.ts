import { Injectable, Injector } from '@angular/core';
import { BaseService } from '../shared/service/base.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService extends BaseService {

  constructor(injector: Injector) {
    super(injector);
  }
  getServiceData() {
    return this.http.get(`${this.apiUrl}home-content`);
  }
}
