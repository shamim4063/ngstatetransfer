import { Component, OnInit } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';
const HOME_KEY = makeStateKey('homekey');

import { HomeService } from './home.service';
import { SeoContentService } from '../shared/service/seo-content.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _homeService: HomeService,
    private state: TransferState,
    private _seoContentService: SeoContentService
  ) { }
  home: any[] = [];
  hContent: any;
  ngOnInit() {
    this._seoContentService.setContent("Boibazar", "Largest Book Stote in Bangladesh", "Largest,Online,Book,Store", 'Slug', 'No Image');
    
    this.hContent = this.state.get(HOME_KEY, null as any);
if (!this.hContent) {
  this._homeService.getServiceData().subscribe((data: any) => {
    this.home = data.topCategorieTab[0].products;
    this.state.set(HOME_KEY, data as any);
  },
    err => console.error(err),
    () => console.log('Home Data Loading Finish'))
} else {
  this.home = this.hContent.topCategorieTab[0].products;
}
  }

}
